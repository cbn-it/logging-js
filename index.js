"use strict";
const {Logging} = require('@google-cloud/logging');
const {makeMiddleware} = require("./make-middleware");

Error.stackTraceLimit = 30;
//https://groups.google.com/forum/#!msg/google-stackdriver-discussion/xRhmJtpR1b0/to7vhhEFAgAJ
//https://cloud.google.com/appengine/articles/logging#linking_app_logs_and_requests

function logginMiddleware(projectId){
    projectId = projectId || process.env.GOOGLE_CLOUD_PROJECT;
    let appName = process.env.APP_NAME || process.env.GAE_SERVICE;
    let podId = process.env.MY_POD_NAME || process.env.GAE_VERSION;
    let version = process.env.APP_VERSION || process.env.GAE_VERSION;
    if (appName === undefined) {
        appName = "localhost";
        podId = "localhost";
        version = "localhost";
    }
    version = version.replace(/.*:/,"");

    const metadata = {
        resource: {
            type: "api",
            labels: {
                "project_id": projectId,
                "service": appName,
                "method": version,
                "version": podId,
                "location": "global"
            }
        }
    };

    const logging = new Logging({
        projectId: projectId,
    });
    const logName = 'my-log';
    const logger = logging.log(logName);
    const logger_child = logging.log(logName + "_child");

    return makeMiddleware(projectId, logger, logger_child, metadata)
}
module.exports.logginMiddleware = logginMiddleware;



