const severityLevels = [
    "DEFAULT",
    "DEBUG",
    "INFO",
    "NOTICE",
    "WARNING",
    "ERROR",
    "CRITICAL",
    "ALERT",
    "EMERGENCY",
];

class Logger {
    constructor(projectId,traceId,logger,logger_child, metadata){
        this.metadata=metadata;
        this.projectId = projectId;
        this.traceId = traceId;
        this.logger = logger;
        this.logger_child = logger_child;

        this.requestStartMs = Date.now();
        this.logs = [];
        this.tags = {};
        this.lines = [];
    }
    getStack(){
        let orig = Error.prepareStackTrace;
        Error.prepareStackTrace = function(_, stack) {
            return stack;
        };
        let err = new Error;
        Error.captureStackTrace(err, this.log);
        let stack = err.stack;
        //console.log(stack[0]);
        Error.prepareStackTrace = orig;
        return stack;
    }
    getLocation(){
        let stack = this.getStack();
        return [stack[1].getFileName(),stack[1].getLineNumber(),stack[1].getFunctionName()];
    }
    log(logLevel, ...args) {
        let m = args;
        if(m.length===1){
            m=m[0];
        }
        if(m.length===1){
            m=m[0];
        }
        if (process.env.NODE_ENV !== "production") {
            console.log(m);
        } else {
            let [file, line, fct] = this.getLocation();
            if (m instanceof Error) {
                m = m.stack
            } else if (typeof m === "string") {
                m = [file, line, fct].join(" ") + ": " + m;
                this.lines.push(m);
            } else {
                m = JSON.stringify(m, null, 2);
                m = [file, line, fct].join(" ") + ": " + m;
            }
            const entry = this.logger_child.entry(Object.assign({}, this.metadata, {
                trace: this.traceId,
                severity: logLevel,
                sourceLocation:{
                    file:file,
                    line:line+"",
                    function:fct
                },
                timestamp: {
                    seconds: Math.floor(Date.now() / 1e3),
                    nanos: Math.floor((Date.now() % 1e3) * 1e6)
                }
            }), m);
            this.logs.push(entry)
        }

    }


    getChildLogger(){
        return {
            d: (...args) => {
                this.log("DEBUG", args);
            },
            i: (...args) => {
                this.log("INFO", args);
            },
            w: (...args) => {
                this.log("WARNING", args);
            },
            s: (...args) => {
                this.log("ERROR", args);
            },
            tag: (tagName, tagValue) => {
                this.tags[tagName] = tagValue;
            }
        }
    }
    makeHttpRequestData(req, res) {
        const latencyMs = Date.now() - this.requestStartMs;
        return {
            status: res.statusCode,
            requestUrl: req.protocol + "://" + req.get('host') + req.originalUrl,
            requestMethod: req.method,
            userAgent: req.headers['user-agent'],
            referer: req.get("Referer"),
            remoteIp: req.connection.remoteAddress,
            protocol: req.protocol,
            responseSize: (res.getHeader && Number(res.getHeader('Content-Length'))) || 0,
            latency: {
                seconds: Math.floor(latencyMs / 1e3),
                nanos: Math.floor((latencyMs % 1e3) * 1e6)
            }
        };
    }
    async emitRequestLog(req, res) {
        let httpRequest = this.makeHttpRequestData(req, res);

        let maxSeverity = this.logs.reduce(function (a, b) {
            return severityLevels[Math.max(severityLevels.indexOf(a), severityLevels.indexOf(b.metadata.severity))]
        }, severityLevels[0]);
        if (httpRequest.status >= 500) {
            maxSeverity = "CRITICAL";
        } else if ([408, 400].includes(httpRequest.status)) {
            if (severityLevels.indexOf(maxSeverity) < severityLevels.indexOf("WARNING")) {
                maxSeverity = "WARNING";
            }
        } else if (httpRequest.status >= 400) {
            maxSeverity = "ERROR";
        }
        const entry = this.logger.entry(Object.assign({}, this.metadata, {
            httpRequest: httpRequest,
            trace: this.traceId,
            severity: maxSeverity,
            timestamp: {
                seconds: Math.floor(this.requestStartMs / 1e3),
                nanos: Math.floor((this.requestStartMs % 1e3) * 1e6)
            }
        }), {
            tags:this.tags,
            lines:this.lines
        });
        try{
            if (process.env.NODE_ENV !== "production") {
                // console.log(httpRequest);
            } else {
                await this.logger.write(entry);
                if (this.logs.length > 0) {
                    await this.logger_child.write(this.logs);
                }
            }
        }catch (err){
            console.error('ERROR:', err);
        }
    }
}
module.exports.Logger = Logger;