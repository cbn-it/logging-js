"use strict";


Object.defineProperty(exports, "__esModule", { value: true });
const onFinished = require("on-finished");
const context_1 = require("@google-cloud/logging/build/src/utils/context");
const {Logger} = require("./logger");

/**
 * Generates an express middleware that installs a request-specific logger on
 * the `request` object. It optionally can do HttpRequest timing that can be
 * used for generating request logs. This can be used to integrate with logging
 * libraries such as winston and bunyan.
 *
 * @param projectId Generated traceIds will be associated with this project.
 * @param loggerParam
 * @param loggerChildParam
 * @param metadata
 */
function makeMiddleware(projectId, loggerParam, loggerChildParam, metadata) {
    return (req, res, next) => {
        const wrapper = context_1.makeHeaderWrapper(req);
        const traceContext = context_1.getOrInjectContext(req,projectId, true );
        let spanId = traceContext.spanId;
        let traceSampled = traceContext.traceSampled;

        const trace = traceContext.trace;
        // Install a child logger on the request object.
        const logger = new Logger(projectId,trace,loggerParam,loggerChildParam, metadata);
        req.log = logger.getChildLogger(trace);

        // Emit a 'Request Log' on the parent logger.
        onFinished(res, (err) => {
            if (err !== null) {
                req.log.w(err);
            }
            logger.emitRequestLog(req, res);
        });
        next();
    };
}
exports.makeMiddleware = makeMiddleware;
//# sourceMappingURL=make-middleware.js.map